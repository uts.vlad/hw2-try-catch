const books = [
    {
        author: "Скотт Бэккер",
        name: "Тьма, что приходит прежде",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Воин-пророк",
    },
    {
        name: "Тысячекратная мысль",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Нечестивый Консульт",
        price: 70
    },
    {
        author: "Дарья Донцова",
        name: "Детектив на диете",
        price: 40
    },
    {
        author: "Дарья Донцова",
        name: "Дед Снегур и Морозочка",
    }
];



function CreateBlock (bookArr) {

    this.booksValidityCheck = function (errorList){
        console.log(errorList);
        throw new Error(`the above book has been excluded from list!`);
    };

    this.sortBookItems = (arrElem) => {
        const {author, name, price} = arrElem;
        if (!(author === undefined) && !(name === undefined) && !(price === undefined)) return true;
        else {
            try {
                this.booksValidityCheck(arrElem)
            } catch (e) {
                console.error(e.message);
            }
            return false;
        }
    };

    this.reSortBooks = function () {
        return bookArr.filter(this.sortBookItems);
    };

    this.createBlockElements = function () {
        const wrapLiElement = (el)=>{
            let {author, name, price} = el;
            return `<li> 
<p>${author}</p><p>${name}</p><p>${price}</p> 
</li>`;
        };
        this.root = document.getElementById('root');
        this.block = document.createElement('ul');
        this.liElements = this.newBooks.map(wrapLiElement).join(' ');
        debugger
        this.block.insertAdjacentHTML('afterbegin', this.liElements);

    };


    this.render = function () {
        this.newBooks = this.reSortBooks();
        this.createBlockElements(this.newBooks); //TODO потом убрать передачу this.newBooks как аргумента
        this.root.append(this.block);
    };

    this.render();
}

const myUlList = new CreateBlock(books);
console.log('myUlList.newBooks', myUlList.newBooks);


